Code was provided from David Herzig. <br>

## Authors 
Fabio Ruckstuhl <br>
Banujan Ragunathan <br>
Andreas Heule <br>

# SDCService
## The Goal of this Projekt
The Goal was as communicated during lecture: <br>
Exercise - Maven Build Process I <br>
1. Clone repository:
git clone https://gitlab.fhnw.ch/david.herzig/sdcservice
2. Install Maven
https://maven.apache.org/
3. Execute Maven Goals:
mvn clean package
4. Run the application:
java -jar target/FILE.jar
5. Use a REST client (e.g. Postman)
6. Create requests for <br>
    ▶ greeting <br>
    ▶ experiment <br>
    ▶ data <br>
7. Open project in IDE (Eclipse: Import, IntelliJ: Open)
8. Implement delete method
9. Implement storage of data (e.g. file)

### Work done:
Implemented following Functions: <br>
**Delete function**: delete objects (experiments) which are in an array list. <br>
**Load function**: Access data coming from Rest API.


## log files
here are all log files stored: <br>
https://gitlab.fhnw.ch/doris/sdcservice/-/blob/main/sdc-service-2023-05-25.0.log <br>
https://gitlab.fhnw.ch/doris/sdcservice/-/blob/main/sdc-service-2023-05-26.0.log <br>
https://gitlab.fhnw.ch/doris/sdcservice/-/blob/main/sdc-service-2023-06-11.0.log <br>
https://gitlab.fhnw.ch/doris/sdcservice/-/blob/main/sdc-service-2023-06-12.0.log <br>
https://gitlab.fhnw.ch/doris/sdcservice/-/blob/main/sdc-service-2023-06-13.0.log <br>
https://gitlab.fhnw.ch/doris/sdcservice/-/blob/main/sdc-service.log <br>
https://gitlab.fhnw.ch/doris/sdcservice/-/blob/main/gpio-service.log <br>







 


