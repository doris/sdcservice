package ch.fhnw.restservice;

public class Experiment {

    private Long id;
    private String name;
    private Long timestamp;

    public Experiment() {
        timestamp = System.currentTimeMillis();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getTimestamp() {
        return timestamp;
    }
}
