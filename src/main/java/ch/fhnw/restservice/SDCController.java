package ch.fhnw.restservice;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import ch.qos.logback.core.net.SyslogOutputStream;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.web.exchanges.HttpExchange;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class SDCController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    private static final Logger LOG = LoggerFactory.getLogger(SDCController.class);

    @PostConstruct
    private void init() throws Exception {
        // insert some sample data in the test version
        String name = "sample_experiment_";
        for (int i = 0; i < 5; i++) {
            DataContainer.getInstance().createExperiment(name + i);
        }
    }

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) throws Exception {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    //Register the element in a list as data(BR)
    @PostMapping("/experiment")
    public Boolean createExperiment(@RequestParam(value = "name") String name) {
        try {
            Boolean result = DataContainer.getInstance().createExperiment(name);
            LOG.info("create new experiment " + name + ": " + result);
            return result;
        } catch (Exception ex) {
            String msg = "could not create experiment with name " + name;
            LOG.error(msg, ex);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, msg, ex);
        }

    }

    //Get all id's as list
    @GetMapping("/experiments")
    public List<Experiment> getExperimentIds() {
        return DataContainer.getInstance().getAllExperiments();

    }

    //Get the ID of the name place in the list (BR)
    @GetMapping("/experiment")
    public Experiment getExperiment(@RequestParam(value = "name") String name) {
        try {
            Experiment experiment = DataContainer.getInstance().getExperiment(name);
            LOG.info("retrieve experiment for " + name + ": " + experiment);
            return experiment;
        } catch (Exception ex) {
            String msg = "could not get experiment for name " + name;
            LOG.error(msg, ex);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, msg, ex);
        }
    }

    /*
    Get the full list as object of the saved elements in the list  (BR)
     */
    @GetMapping("/data")
    public List<DataObject> getData(@RequestParam(value = "experimentid") Long experimentId) {
        List<DataObject> result = DataContainer.getInstance().getData(experimentId);
        LOG.debug("Data Result Size = " + result.size());
        return result;
    }

    /*
    Post the object with the data (BR)
    be aware: you have to post first than you can retrieve with REST command above.
     */
    @PostMapping("/data")
    public void storeData(@RequestBody DataObject dObj) {
        if (dObj == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "data object is null");
        }

        LOG.info("store data for experiment " + dObj.getExperimentId());
        try {
            DataContainer.getInstance().storeData(dObj);
        } catch (Exception ex) {
            LOG.error("store data for experiment " + dObj.getExperimentId() + ": " + ex.getMessage(), ex);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage(), ex);
        }
    }

    //Delete experiment element in a list (BR)
    @PostMapping("/delete")
    public void deleteExperiment(@RequestParam(value = "experimentid") Long experimentId) {
        List<Experiment> results = DataContainer.getInstance().getAllExperiments();
        Experiment experimentToRemove = new Experiment();
        boolean deleted = false;

        try {
            for (Experiment result : results) {
                if (result.getId().equals(experimentId)) {
					experimentToRemove = result;
					deleted = true;
					break;
				}

            }
            if (deleted) {
                DataContainer.getInstance().deleteExperiment(experimentToRemove);
                LOG.info("Number " + experimentId + " has been deleted from dataset. Delete status: true");
            } else {
				String problem = "";
				if (results.size() > experimentId || results.size() < experimentId) {
					problem = "Your request dataset "+ experimentId+ " is not available. Your selected ID is not found";
					LOG.debug("" + problem);
				}
			}

        } catch (Exception ex) {
            String msg = "could not delete experiment number with experimentId " + experimentId;
            LOG.error(msg, ex);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, msg, ex);
        }

    }

    @PostMapping("/file")
    public void storeData(@RequestParam(value = "filename") String filename) {
        LOG.info("store current data into file " + filename);
        try {
            //DataContainer.getInstance().load(filename); //Testing this method if its work (BR)
            DataContainer.getInstance().saveCSV(filename);
        } catch (Exception ex) {
            LOG.error("save data into " + filename + " failed! " + ex.getMessage());
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
        }
    }


    public void reset() {

    }
}
