package ch.fhnw.restservice;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

/**
 * @author David Herzig
 * This class acts as a container to store all experiments and all data objects.
 */
public class DataContainer {

    private static final Logger LOG = LoggerFactory.getLogger(DataContainer.class);

    private static DataContainer instance;
    private final AtomicLong idgen = new AtomicLong();

    private Map<Experiment, List<DataObject>> data = new HashMap();

    public static DataContainer getInstance() {
        if (instance == null) {
            instance = new DataContainer();
        }
        return instance;
    }

    private DataContainer() {
        // singleton
    }

    /**
     * Get all the experiments.
     *
     * @return As said in description.
     */
    public List<Experiment> getAllExperiments() {
        List<Experiment> result = new ArrayList<>();
        result.addAll(data.keySet());
        return result;
    }

    /**
     * @param name
     * @return
     * @throws Exception
     */
    public Boolean createExperiment(String name) throws Exception {
        Experiment experiment = getExperiment(name);
        if (experiment == null) {
            Long identifier = idgen.incrementAndGet();
            experiment = new Experiment();
            experiment.setName(name);
            experiment.setId(identifier);
            List<DataObject> dataContainer = new ArrayList<>();
            data.put(experiment, dataContainer);
            return true;
        } else {
            LOG.warn("Experiment " + name + " already exist!");
        }
        return false;
    }

    private List<Long> getExperimentIds() {
        List<Long> result = new ArrayList<>();
        for (Experiment ex : data.keySet()) {
            result.add(ex.getId());
        }
        return result;
    }

    public Experiment getExperiment(String name) throws Exception {
        for (Experiment ex : data.keySet()) {
            if (ex.getName().equals(name)) {
                return ex;
            }
        }
        return null;
    }

    public Experiment getExperiment(Long id) throws Exception {
        for (Experiment ex : data.keySet()) {
            if (ex.getId().equals(id)) {
                return ex;
            }
        }
        return null;
    }

    public void storeData(DataObject dObj) throws Exception {
        // check if data object is not null
        if (dObj == null) {
            throw new Exception("provided data object is null");
        }

        // check if experiment exist
        Experiment experiment = getExperiment(dObj.getExperimentId());
        if (experiment == null) {
            throw new Exception("experiment id " + dObj.getExperimentId() + " does not exist!");
        }

        // populate experiment name
        dObj.setExperimentName(experiment.getName());

        List<DataObject> dataObjects = data.get(experiment);
        if (dataObjects == null) {
            dataObjects = new ArrayList<DataObject>();
            data.put(experiment, dataObjects);
        }
        dObj.setId(idgen.incrementAndGet());

        dataObjects.add(dObj);
    }

    public List<DataObject> getData(Long experimentId) {
        return data.get(experimentId);
    }

    //Function of DataContainer to remove the data in the list (BR)
    public void deleteExperiment(Experiment experiment) throws Exception {
        if (experiment == null) {
            throw new Exception("Experiment is null!");

        }
        if (!data.containsKey(experiment)) {
            throw new Exception("experiment id " + experiment.getName() + " does not exist!");
        }
        //Remove experiement from dataset (BR)
        data.remove(experiment);
    }

    //Load external file as json (already Gson dependency available) (BR)
    public void load(String filename) throws Exception {
        LOG.debug("Imported File = " + filename);

        if (filename.isEmpty()) {
            throw new Exception("provided filename is not valid or empty");
        }

        //GSON instance to parse to JSON (BR)
        Gson gson = new Gson();

        //set file name with "data" (BR)
        String baseDir = "data";

        File file = new File(baseDir + "/" + filename);

        //every single line will read and attached (BR)
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

            //One String without multiple String Object (BR)
            StringBuilder json = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                json.append(line);
                //Convert JSON Text to Object Datacontainer with GSON library (BR)
                DataContainer newContainer = gson.fromJson(json.toString(), DataContainer.class);

                //making use of directly instance because Datacontainer instance (singleton) - reload container (BR)
                instance = newContainer;
                reader.close();

            }
        } catch (FileNotFoundException e) {
            LOG.error("File not found: " + filename);
        }
    }

    public void saveJSON(String filename) throws Exception {
        String baseDir = "data";
        Gson gson = new Gson();
        String json = gson.toJson(data);
        BufferedWriter bw = new BufferedWriter(new FileWriter(baseDir + "/" + filename));
        bw.write(json);
        bw.close();
    }

    public void saveCSV(String filename) throws Exception {
        String baseDir = "data";
        File fout = new File(baseDir + "/" + filename);
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (Experiment experiment : data.keySet()) {
            List<DataObject> dObjs = data.get(experiment);
            for (DataObject dObj : dObjs) {
                String line = "" + dObj.getExperimentId() + ";" +
                        dObj.getExperimentName() + ";" +
                        dObj.getSensorId() + ";" +
                        dObj.getSensorType() + ";" +
                        dObj.getAccuracy() + ";" +
                        dObj.getId() + ";" +
                        dObj.getTimestamp() + ";";
                if (dObj.getData() != null) {
                    for (float d : dObj.getData()) {
                        String fLine = line + d;
                        bw.write(fLine);
                        bw.newLine();
                    }
                }
            }
        }
        bw.close();
        fos.close();
    }
}
